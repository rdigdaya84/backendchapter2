package topik5;

import java.util.*;

public class map {
    public static void main(String[] args){
//        belajarMap1();

        System.out.println("\nCode 2");
        belajarMap2();  //dengan iterator

        System.out.println("\nCode 3");
        belajarMap3(); // tanpa iterator

        System.out.println("\nCode 4");
        belajarMap4(); // tanpa iterator
    }

    private static void belajarMap4() {
        Map<String, String> mapCountryCodes = new HashMap();

        // menambahkan isi
        mapCountryCodes.put("1", "USA");
        mapCountryCodes.put("44", "United Kingdom");
        mapCountryCodes.put("33", "France");
        mapCountryCodes.put("81", "Japan");

//        mendapatkan key dan value dari sebuah method.
        Set<Map.Entry<String, String>>  entries = mapCountryCodes.entrySet();

        for (Map.Entry<String, String> entry : entries){
            String code = entry.getKey();
            String country = entry.getValue();

            System.out.println(code + " => " + country);
        }

    }

    private static void belajarMap3() {
        Map<Integer, String> mapHttpErrors = new HashMap();

        // menambahkan isi
        mapHttpErrors.put(200, "OK");
        mapHttpErrors.put(303, "See Other");
        mapHttpErrors.put(404, "Not Found");
        mapHttpErrors.put(500, "Internal Server");

        //tanpa iterator
        for(Integer i : mapHttpErrors.keySet()){
            System.out.println(mapHttpErrors.get(i));
        }
    }

    private static void belajarMap2() {
        Map<String, String> mapCountryCodes = new HashMap();

        // menambahkan isi
        mapCountryCodes.put("1", "USA");
        mapCountryCodes.put("44", "United Kingdom");
        mapCountryCodes.put("33", "France");
        mapCountryCodes.put("81", "Japan");

//        perulangan denga iterator
        Set<String>  setCodes = mapCountryCodes.keySet();
        Iterator<String> iterator = setCodes.iterator();

        while (iterator.hasNext()){
            String code = iterator.next();
            String country = mapCountryCodes.get(code);

            System.out.println(code + " => " + country);
        }

    }

    private static void belajarMap1() {
        Map<String, String> alamat = new HashMap();
        alamat.put("Pak RIski", "10, 10");
        alamat.put("Pak Agus", "58, 46");
        alamat.put("Mas Joko", "10, 10");

        List<String> rumahYgSamaan = new ArrayList<String>();
        Set<Map.Entry<String, String>> entries = alamat.entrySet();
        for(Map.Entry<String, String> element : entries){
            if(element.getValue().equals("10,10")){
                rumahYgSamaan.add(element.getKey());
            }
        }

        for(String pemilik: rumahYgSamaan){
            System.out.print(pemilik);
        }
    }
}
