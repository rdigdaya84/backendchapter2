package topik5;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class list {
    public static void main(String[] args){
        topik5();
    }

    private static void topik5() {
        // array biasa / array tradisional
        int[] intArray = new int[5];
        for(int i=0; i<intArray.length; i++){
            intArray[i] = i;
        }


        //collection / array termodernisasi
        List<Integer> ListArray = new ArrayList();
        //harus menggunakan integer object
        //kita tidak perlu mendefinisikan panjangnya


        //tambah element berbentuk integer ke listArray
        for(int i=0; i<5; i++){
            ListArray.add(i);
        };
        tampilListArray(ListArray);


        //mengubah nilai list
        ListArray.set(0, 1000);
        tampilListArray(ListArray);

        //menghapus nilai list
        ListArray.remove(3);
        tampilListArray(ListArray);

        List<String> perabot = Arrays.asList(new String[]{"Kursi", "Meja", "Lemari", "Ember", "Gayung"});
        for(String isi: perabot){
            System.out.print(isi + " ");
        }
    }

    public static void tampilListArray(List ListArray){
        //mengambil nilai panjang list dengan method    .size()
        for(int i=0; i<ListArray.size(); i++){
            //menampilkan nilai berdasarkan element     .get(element)
            System.out.print(ListArray.get(i) + " ");
        };

        System.out.println(" ");

        //ekuivalen / sama dengan   for each
//        for(Object s: ListArray){
//            System.out.print(s + " ");
//        }
    }

}
