package topik1;

import java.util.Scanner;

public class topik1b {
    public static void main(String[] args){

        System.out.println("program untuk menampilkan game FizzBuzz");
        Scanner inputPanjang = new Scanner(System.in);

        System.out.print("Masukan panjang yang diharapkan : ");
        int panjang = inputPanjang.nextInt();

        for(int i=1; i<=panjang; i++){
            if( i % 3 == 0 && i % 5 == 0) {
                System.out.print("FizzBuzz ");
            }
            else if(i % 3 == 0 ){
                System.out.print("Fizz ");
            }else if(i % 5 == 0 ){
                System.out.print("Buzz ");
            }else{
                System.out.print(i + " ");
            }
        }
    }
}
