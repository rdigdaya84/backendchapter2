package topik1;

public class topik1a {

    public static void main(String[] args){
        System.out.println("mengurutkan array berikut mulai dari yang terkecilke yang terbesar");


        int myArray[] = {100, 20, 15, 30, 5, 75, 40};

        for(int i=0; i<myArray.length; i++){
            for(int j=0; j< myArray.length-i-1; j++){
                if(myArray[j] > myArray[j+1]){
                    int temp = myArray[j];
                    myArray[j] = myArray[j+1];
                    myArray[j+1] = temp;
                }
            }
        }

        for( int i=0; i < myArray.length; i++){
            System.out.print(myArray[i] + " ");
        }
    }
}
