package topik1;

import java.util.Scanner;

public class topic1c {

    public static void main(String[] args) {
        System.out.println("program untuk mengidentifikasi sebuah input");

        Scanner inputText = new Scanner(System.in);
        System.out.print("Masukkan Text : ");
        String text = inputText.nextLine();

        char[] pisahText = text.toCharArray();
        int n = pisahText.length;
        int palindrome = 0;

        for (int i = 0; i < n; i++) {
//            System.out.print(pisahText[i]);
//            System.out.print(pisahText[n-1-i]);

            if(pisahText[i] == pisahText[n-1-i]){
//                System.out.println(pisahText[i]+"+"+ pisahText[n-1-i]);
                palindrome += 1;
            }
        }

        if(palindrome == n){
            System.out.println("Output: String is palindrome");
        }else{
            System.out.println("Output: String is not palindrome");
        }
    }
}
