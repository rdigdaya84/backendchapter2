package topik3.model;

public class Mobil {

    private String merk;
    private String model;
    private String warna;

//    Getter and Setter
    public String getMerk() {
        return merk;
    }
    public void setMerk(String merk) {
        this.merk = merk;
    }
    public String getModel() {
        return model;
    }
    public void setModel(String model) {
        this.model = model;
    }
    public String getWarna() {
        return warna;
    }
    public void setWarna(String warna) {
        this.warna = warna;
    }


    //method Overload
    public void startMesin(){
        System.out.print("Mesin di stater");
    }
    public void startMesin(String lokasiStart){
        System.out.print("Mesin di stater di " + lokasiStart);
    }
    public void startMesin(String lokasiStart, String lokasiTUjuan){
        System.out.print("Mesin di stater di " + lokasiStart + "pergi ke " + lokasiTUjuan);
    }
}
