package topik3.model;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class LombokLibrary {
    private String nama;
    private int umur;
    private String jenisKelamin;
}
