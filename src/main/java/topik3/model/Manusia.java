package topik3.model;

public class Manusia {

    // ini Field
//    public String tubuh;

    //menerapkan akses modifier ke field
    private String tubuh;

//    constructor
    public Manusia(){
        System.out.print("Hai,");
    }

//    constructor dengan parameter
    public Manusia(String nama){
        System.out.println("Hai," + nama);
    }

//    contoh modifier static
    public static void printManusia(String nama){
        System.out.println("Hai," + nama);
    }

    public String getTubuh(){
        return tubuh;
    }
    public void setTubuh(String tubuh){
        this.tubuh = tubuh;
        //this.tubuh mengacu ke variabel global
        //tubuh mengacu ke variabel dari parameter
    }
}
