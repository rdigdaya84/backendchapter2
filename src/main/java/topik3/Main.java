package topik3;

import topik3.model.Manusia;

public class Main {
    public static void main(String[] args){

//        inisialisasi object dari class Manusia
        Manusia agus = new Manusia();
        agus.setTubuh("tinggi");
        System.out.println(agus.getTubuh());

//        inisialisasi object dalam class manusia constructor dengan parameter
        Manusia riski = new Manusia("risky");

//        contoh instance modifier static
        Manusia.printManusia("wahyu");
    }
}
