package topik4.model;

public class StaticFinal {

    public void contohMethod(){
        // dengan static
        InnerClassStatic.methodPrint();

        //InnerClass tanpa Static
        InnerClass innerClass =  new InnerClass();
        innerClass.methodPrint();
        // atau bisa aja
        new InnerClass().methodPrint();
    }

    //gak perlu instance dulu
    public static class InnerClassStatic{
        public static void methodPrint(){
            System.out.print("ping!");
        }
    }

    public class InnerClass{
        public void methodPrint(){
            System.out.print("beep!");
        }
    }
}
