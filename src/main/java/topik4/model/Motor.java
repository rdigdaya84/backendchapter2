package topik4.model;

/*
abstract class diperbolehkan punya sebuah field dan constructor.
 */

public abstract class Motor {

    private String merkMotor;
    static final String merkMobil = "Honda";

    public abstract boolean startMesin();
    public abstract void setirMotor();
    public abstract void rem();

    public void kelengkapan(){
        System.out.println("STNK ada");
        System.out.println("SIM ada");
        System.out.println("Razia tidak ada");
    }
}
