package topik4.model;

import lombok.Getter;

@Getter
public enum contohEnum {
    PHI(3.14),
    HARI_SEMINGGU(7),
    BULAN_SETAHUN(12);

    private double value;

    private contohEnum(double value){
        this.value = value;
    }
}


//System.out.println(contohEnum.PHI.getValue));