package topik4.model;

//sifatnya selalu static final
// static final ga perlu ditulis, karena udah otomatis dalam interface
// static final = constant

public interface Mobil {
    static final String MERK_MOBIL = "Honda";
    String WARNA = "Hitam";

//    public abstract void startMesinMulai();
    void startMesin();
    String supir();

//    default void test(){
//
//    }


    class Motor {

    }
}
