package topik4.oop;


//Superclass
//Abstract : sebuah aspek yang berfungsi untuk menyembunyikan detail dari suatu aktivitas terhadap user.
public abstract class Hewan {
    //Field dari SuperClass
    public String jenis = "Hewan ";
    //Method dari SuperClass
    public abstract void suaraHewan();
}
