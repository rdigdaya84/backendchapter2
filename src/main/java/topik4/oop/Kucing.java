package topik4.oop;

import lombok.Data;

//Subclass
//Inheritance
public class Kucing extends Hewan{

    //Constructor
    public Kucing(){
        System.out.print(jenis);
        System.out.println("Kucing");
    }

    //Construcor dengan parameter
    public Kucing(String status){
        System.out.println("Kucing " + status);
    }

    //Overriding method
    public void suaraHewan() {
        System.out.println("Kucing bersuara : weong\n");
    }

}
