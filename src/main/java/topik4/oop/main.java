package topik4.oop;
//Encapsulation : prinsip pada OOP untuk menyembunyikan field dari class lainnya
public class main {
    public static void main(String[] args){

//        Inheritance

        // SuperClass
        Hewan hewanPeliharaan = new Hewan() {
            @Override
            public void suaraHewan() {
                System.out.println("Hewan bersuara\n");
            }
        };
        hewanPeliharaan.suaraHewan();

        //Sub Class
        Hewan kucingPeliharaan = new Kucing();
        kucingPeliharaan.suaraHewan();
        new Kucing("Liar").suaraHewan();        // Constructor dengan Parameter



        //Polymorphism
        Tikus tikusPeliharaan = new Tikus();
        tikusPeliharaan.Lari();
        tikusPeliharaan.Lari("Dapur");



    }
}
