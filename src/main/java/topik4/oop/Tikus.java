package topik4.oop;

//Subclass
//Polymorphism : adalah prinsip yang memungkinkan sebuah method memiliki berbagai bentuk
public class Tikus extends Hewan {

    //Overriding method : penggunaan method dengan nama yang sama, tapi pake implementasi method yang berbeda
    public void suaraHewan() {
        System.out.println("Tikus bersuara : cit cit\n");
    }

    //Overloading Method : penggunaan method dengan nama yang sama, tapi pakai parameter yang berbeda atau jumlah parameter yang berbeda
    public void Lari(){
        System.out.println("Tikus Mulai Berlari");
    }
    public void Lari( String tempat ){
        System.out.println("Tikus Mulai Berlari ke " + tempat);
    }

}
