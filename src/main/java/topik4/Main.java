package topik4;

import topik4.model.contohEnum;
import topik4.pilar.Kendaraan;
import topik4.pilar.Tronton;
import topik4.pilar.Truk;

public class Main {

    public static void main(String[] args){

        Kendaraan kendaraan = new Kendaraan(){

            public int kapasitasPenumpang() {
                return 0;
            }

            public int jumlahRoda() {
                return 0;
            }

            public String platNomor() {
                return null;
            }

            public String warna() {
                return null;
            }

            public String merk() {
                return null;
            }
        };

        Tronton tronton = new Tronton();
        tronton.isNyala = true;

        Kendaraan kendaraan1 = new Tronton();
        kendaraan1.isNyala = false;
        kendaraan1 = new Truk() {
            public String merk() {
                return null;
            }

            public double berat() {
                return 0;
            }
        };



//        Pengguaan Enum
        System.out.print(contohEnum.PHI.getValue());
    }
}
