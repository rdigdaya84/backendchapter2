package topik4.pilar;

public abstract class Kendaraan {

    public boolean isNyala = true;

    public abstract int kapasitasPenumpang();
    public abstract int jumlahRoda();
    public abstract String platNomor();
    public abstract String warna();
    public abstract String merk();

    public void klakson(){
        System.out.println("beep beep");
    }
}
