package topik4.pilar;
import topik4.model.Mobil;

/*
      Abstract menggunakan extends
    inheritance suatu interface menggunakan implements

// extends max 1 abstract class, implements bisa banyak interface
 */



public abstract class Truk extends Kendaraan implements Mobil {

    public abstract double berat();

    public Truk(){
        System.out.print("Truk nyala : " + isNyala);
        System.out.print("Kapasitas Penumpang : " + kapasitasPenumpang());
        System.out.print("Jumlah Roda : " + jumlahRoda());

    }

    public int kapasitasPenumpang() {
        return 3;
    }

    public int jumlahRoda() {
        return 6;
    }

    public String platNomor() {
        return "B 1234 MT";
    }

    public String warna() {
        return "Kuning";
    }



    // implementasi method abstract dari interface Mobil
    public void startMesin() {
        System.out.println("Start Mesin Truk!");
    }
    public String supir() {
        return "Akoe";
    }


}
