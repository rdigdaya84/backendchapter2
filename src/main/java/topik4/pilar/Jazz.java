package topik4.pilar;

import topik4.model.Mobil;

public class Jazz implements Mobil {

    //  untuk field tidak bisa di override
//    MEREK_MOBIL = "nama baru"

    static final String model = "Jazz";

    public void startMesinMulai() {
        System.out.print(MERK_MOBIL);
    }

    public void startMesin() {
        System.out.print(MERK_MOBIL);
    }

    public String supir() {
        return "Riski";
    }
}
